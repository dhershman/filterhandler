[![Build Status](https://travis-ci.org/dhershman1/FilterHandler.svg?branch=master)](https://travis-ci.org/dhershman1/FilterHandler)
## Info
v1.0.0 offers a complete re write of filterhandler
It will now function more data based rather than event based. It's primary purpose is getting data and filtering it as expected and sending it back. It will keep your views as views, while also allowing them to keep data updated the module will clone data passed to it, so your original data will always be left intact and never mutated.

## How To
You can install filter handler via `npm`
```
npm i filterhandler
```

You can run filterhandlers tests via `npm test` make sure to have mocha and chai installed for this reason.

### Setup
Simply require filterhandler and initialize it with some options
```js
const filterhandler = require('filterhandler')(options)
```

### Options
options include:

- `trackHistory` - keep track of filter and data search history `default: false`
- `searchableData` - the data filterhandler will be managing `send as an array`
- `pageSize` - how many items should filterhandler limit the return to? `default: 10` set to `null` or `0` to disable

### Usage
```js
const filterhandler = require('filterhandler')({
  trackHistory: true,
  searchableData: [{name: 'Bob'}, {name: 'Andrew'}, {name: 'Carson'}]
});

//Click sort event happens
filterhandler.sortBy('name');
//Output: [{name: 'Andrew'}, {name: 'Bob'}, {name: 'Carson'}]
//History: {name: [{name: 'Andrew'}, {name: 'Bob'}, {name: 'Carson'}]}
```

### Public Functions
#### Getters
Getter to get back data from the module

##### Usage
```js
//Type can equal: history or data, if no type is sent get will return both data and history in an object
filterhandler.get();
//Output: {history: historyArray, data: defaultDataArray}
filerhandler.get('history');
//Output: returns the history array the module has built
filterhandler.get('data');
//Output: returns the data array that was set to the module
```

#### search
Searches data and returns an object of data that matches the search

##### Usage
```js
//Data:[{ name: 'Billy', age: 17, status: 'Good' }, { name: 'George', age: 16, status: 'Good' }, { name: 'Samantha', age: 16, status: 'Fantastic' }]
filterhandler.search('Billy');
//Output: [ { name: 'Billy', age: 17, status: 'Good' } ]
```

#### sortBy
Sorts data based on key given also supports sorting by dates

##### Usage
```js
//Data: [{ name: 'Billy', position: 'Kool Kid', grade: 'G' }, { name: 'Albert', position: 'Alpaca', grade: 'Grass' }]
filterhandler.sortBy('name');
// Ouput: [{ name: 'Albert', position: 'Alpaca', grade: 'Grass' }, { name: 'Billy', position: 'Kool Kid', grade: 'G' }]
```
Sorting by a date is also simple however make sure your data contains `date` in the `property` name and your `key` sent also contains `date`.

Dates sort in the following order: First by `Year`, if years are the same it will then try to sort by `Month`, and if all else fails it will attempt to sort by `Day`
Dates should be sent already formatted with `/`, `-`, or `.` to your prefence. for example: `11/14/2016` would be a proper date to send.
```js
//Data: [{ name: 'Kelly', startDate: '09/15/2016' }, { name: 'Ben', startDate: '01/05/2016' }, { name: 'Frank', startDate: '10/14/2012' }]
filterhandler.sortBy('startDate');
//Output: [ { name: 'Frank', startDate: '10/14/2012' }, { name: 'Ben', startDate: '01/05/2016' }, { name: 'Kelly', startDate: '09/15/2016' } ]
```
sort accepts a 2nd argument that will send the array back in a descending order, simply send true as the 2nd argument.
```js
//Data: [{ name: 'Billy', position: 'Kool Kid', grade: 'G' }, { name: 'Albert', position: 'Alpaca', grade: 'Grass' }]
filterhandler.sortBy('name', true);
// Ouput: [{ name: 'Billy', position: 'Kool Kid', grade: 'G' }, { name: 'Albert', position: 'Alpaca', grade: 'Grass' }]
````

#### highlight
Goes through the given data and tries to find and match a provided term to data, it will then place a property of `highlight` within it which will be a `boolean` of `true`

##### Usage
```js
//Data: [{ name: 'Billy', position: 'Kool Kid', grade: 'G' }, { name: 'Albert', position: 'Alpaca', grade: 'Grass' }]
filterhandler.highlight('Grass');
// Ouput: [{ name: 'Billy', position: 'Kool Kid', grade: 'G' }, { name: 'Albert', position: 'Alpaca', grade: 'Grass', highlight: true }]
```

#### offset
Modifies the count of data to give you on offset amount. This is by default called at the end of every function to make sure you get the correct data count back. To disable offset simple set the `pageSize` option either to null or 0

##### Usage
```js
//opts.pageSize: 2
let data = [{ name: 'Kelly', startDate: '09/15/16' }, { name: 'Ben', startDate: 'Janurary 5, 2016' }, { name: 'Frank', startDate: '10/14/12' }]
filterhandler.offset(data);
//Output: [ { name: 'Frank', startDate: '10/14/12' }, { name: 'Ben', startDate: 'Janurary 5, 2016' } ]
```

#### clear
Clears out history and returns both the default data and the empty history object back to the caller as an object

##### Usage
```js
//History: {name: [{name: 'Andrew'}, {name: 'Bob'}, {name: 'Carson'}]}
filterhandler.clear();
//Output: {data: defaultData, history: {}}
```
## Changelog
#### v1.0.1
 - Fix a typo in the `readme`

#### v1.1.0
- Added support for partials in search and highlight
- `get` is now a single function expecting a string or nothing
- Removed moment dependency now expects dates to already be formatted in a MM DD YYYY style the module supports `/, -, and .`
- `offset` will now slice data instead of looping, should boost performance for large data sets.
- `offset` now uses a pageNumber option
- Added `set` functionality to allow the user to set new values in options (such as pageNumber)