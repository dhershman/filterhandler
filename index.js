'use strict';

function filterHandler(options) {
	let defaults = {
		trackHistory: false,
		searchableData: [],
		pageSize: 10,
		pageNumber: 1
	};

	let opts = Object.assign({}, defaults, options);
	let history = {};
	let data = opts.searchableData;

	function get(type) {
		let key = (type) ? type.toLowerCase() : type;

		if (key === 'data') {
			return data;
		} else if (key === 'history') {
			return history;
		}

		return {
			history: history,
			data: data
		};
	}

	function set(key, value) {
		if (!key) return;

		opts[key] = value;

		return opts[key];
	}

	function clone(d) {
		return JSON.parse(JSON.stringify(d));
	}

	function search(term) {
		let regex = new RegExp(term.split('').join('.*'));

		return data.filter(function(item) {
			for (var key in item) {
				if (regex.test(item[key])) {
					if (opts.trackHistory) history[term.toLowerCase()] = item;
					return true;
				}
			}
		});
	}

	function dateSort(dates) {
		let date1 = dates[0].split(/([/|\-|.])/g);
		let date2 = dates[1].split(/([/|\-|.])/g);


		for (let i = date1.length; i >= 0; i--) {
			if (Number(date1[i]) !== Number(date2[i])) {
				return (date1[i] > date2[i]) ? -1 : 1;
			}
		}
	}

	function sortBy(key, descending) {
		let cData = clone(data);

		let sorted = cData.sort(function(a, b) {
			if (key.toLowerCase().indexOf('date') > -1) {
				return dateSort(a[key], b[key]);
			}

			if (a[key] < b[key]) {
				return -1;
			}

			if (a[key] > b[key]) {
				return 1;
			}

			return 0;
		});

		if (descending) sorted.reverse();

		if (opts.trackHistory) history[key] = sorted;

		return sorted;
	}

	function highlight(term) {
		let cData = clone(data);

		return cData.map(function(item) {
			for (var key in item) {
				if (item[key].includes(term)) {
					item.highlight = true;
				}
			}

			return item;
		});
	}

	function offset(items) {
		if (opts.pageSize && opts.pageSize > 0) {
			return items.slice((opts.pageNumber -1)*opts.pageSize, opts.pageSize*opts.pageNumber);
		}

		return items;
	}

	function clear() {
		history = {};

		return {
			data: data,
			history: {}
		};
	}

	return {
		search: search,
		highlight: highlight,
		sortBy: sortBy,
		get: get,
		set: set,
		clear: clear,
		offset: offset
	};
}

module.exports = filterHandler;
