'use strict';
const filterhandler = require('../index.js');
const expect = require('chai').expect;

let mockData = [{
	name: 'Billy',
	position: 'Kool Kid',
	grade: 'G'
}, {
	name: 'Albert',
	position: 'Alpaca',
	grade: 'Grass'
}, {
	name: 'Zeb',
	position: 'House Guy',
	grade: 'Woods'
}];

let mockDates = [{
	name: 'Kelly',
	startDate: '09/15/2016'
}, {
	name: 'Ben',
	startDate: '01/05/2016'
}, {
	name: 'Frank',
	startDate: '10/14/2012'
}];

describe('Functionality Tests', function() {
	let instance;
	beforeEach(function() {
		instance = filterhandler({
			searchableData: mockData,
			pageSize: 0
		});
	});

	describe('Testing get functions', function() {
		describe('Testing get.data()', function() {
			it('should return default data', function() {
				let results = instance.get('data');
				expect(results).to.deep.equal(mockData);
			});
		});

		describe('Testing get.history()', function() {
			it('should return default history', function() {
				let results = instance.get('history');
				expect(results).to.deep.equal({});
			});
		});

		describe('Testing get.all()', function() {
			it('should return both default data & history', function() {
				let results = instance.get();
				expect(results).to.deep.equal({
					data: mockData,
					history: {}
				});
			});
		});
	});

	describe('Testing search()', function() {
		it('should search our data and return the result', function() {
			let results = instance.search('Billy');
			expect(results[0].name).to.equal('Billy');
			expect(results[0].position).to.equal('Kool Kid');
			expect(results[0].grade).to.equal('G');
		});

		it('should do a fuzzy search', function() {
			let results = instance.search('Bly');
			expect(results[0].name).to.equal('Billy');
			expect(results[0].position).to.equal('Kool Kid');
			expect(results[0].grade).to.equal('G');
		});
	});

	describe('Testing sortBy()', function() {
		it('should sort by name', function() {
			let results = instance.sortBy('name');
			expect(results[0].name).to.equal('Albert');
			expect(results[1].name).to.equal('Billy');
		});

		it('shouldnt have mutated our data', function() {
			let results = instance.get('data');
			expect(results[0].name).to.equal('Billy');
		});

		describe('Test sortBy() using dates', function() {
			let dateInstance;
			before(function() {
				dateInstance = filterhandler({
					searchableData: mockDates,
					pageSize: 0
				});
			});

			it('should sort by dates', function() {
				let results = dateInstance.sortBy('startDate');
				expect(results[0].startDate).to.equal('10/14/2012');
			});

			describe('Test clear()', function() {
				it('should return regular data', function() {
					let results = dateInstance.clear();
					expect(results).to.deep.equal({
						data: mockDates,
						history: {}
					});
				});
			});
		});
	});

	describe('Testing highlight()', function() {
		it('should return highlightable item', function() {
			let results = instance.highlight('Grass');
			expect(results[1].highlight).to.be.ok;
		});
	});
});

describe('History Tests', function() {
	let instance;
	before(function() {
		instance = filterhandler({
			trackHistory: true,
			searchableData: mockData,
			pageSize: 0
		});
	});

	it('should save search history', function() {
		instance.search('Billy');
		let results = instance.get('history');
		expect(results.billy).to.be.ok;
		expect(results.billy.position).to.equal('Kool Kid');
	});

	it('should save sort order', function() {
		instance.sortBy('position');
		let results = instance.get('history');
		expect(results.position[0]).to.be.ok;
		expect(results.position[0].position).to.equal('Alpaca');
	});

	describe('Testing clear() with history', function() {
		it('should clear history', function() {
			let result = instance.clear();
			expect(result).to.deep.equal({
				data: mockData,
				history: {}
			});
		});
	});
});

describe('Testing Page Size', function() {
	let instance;
	before(function() {
		instance = filterhandler({
			searchableData: mockData,
			pageSize: 1
		});
	});

	it('should only setup 1 item in searchable data', function() {
		let results = instance.offset(mockData);
		expect(results.length).to.equal(1);
	});
});

describe('Multi Item Search', function() {
	let instance;
	before(function() {
		instance = filterhandler({
			trackHistory: true,
			searchableData: [{
				name: 'Billy',
				age: '17',
				status: 'Good'
			}, {
				name: 'George',
				age: '16',
				status: 'Good'
			}, {
				name: 'Samantha',
				age: '16',
				status: 'Fantastic'
			}],
			pageSize: 0
		});
	});
	it('should return two objects', function() {
		let results = instance.search('Good');
		expect(results.length).to.equal(2);
		expect(results[0].name).to.equal('Billy');
		expect(results[1].name).to.equal('George');
	});
});
